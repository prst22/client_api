<?php
// error_reporting(-1);
ini_set('display_errors', 0);
// ini_set('display_startup_errors', 1);
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require './vendor/autoload.php';

// Define root path
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

require './inc/settings.php';

$app = new \Slim\App($config);

$app->add(new Tuupola\Middleware\JwtAuthentication([
    "secret" => getenv('JWT_SECRET'),
    "ignore" => ["/api/clients/", "/api/user/", "/api/singup"],
    "header" => "Authorization"
]));

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

// Routs
require './inc/routs.php';


// Run app
$app->run();



