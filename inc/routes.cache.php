<?php return array (
  0 => 
  array (
    'POST' => 
    array (
      '/api/user' => 'route1',
      '/api/singup' => 'route2',
      '/api/add/client' => 'route6',
    ),
    'GET' => 
    array (
      '/api/clients' => 'route3',
      '/api/clients/1' => 'route4',
    ),
    'PUT' => 
    array (
      '/api/update/client' => 'route7',
    ),
  ),
  1 => 
  array (
    'OPTIONS' => 
    array (
      0 => 
      array (
        'regex' => '~^(?|/(.+))$~',
        'routeMap' => 
        array (
          2 => 
          array (
            0 => 'route0',
            1 => 
            array (
              'routes' => 'routes',
            ),
          ),
        ),
      ),
    ),
    'GET' => 
    array (
      0 => 
      array (
        'regex' => '~^(?|/api/clients/([^/]+)/([^/]+))$~',
        'routeMap' => 
        array (
          3 => 
          array (
            0 => 'route5',
            1 => 
            array (
              'page' => 'page',
              'sort_by' => 'sort_by',
            ),
          ),
        ),
      ),
    ),
    'DELETE' => 
    array (
      0 => 
      array (
        'regex' => '~^(?|/api/remove/client/([^/]+))$~',
        'routeMap' => 
        array (
          2 => 
          array (
            0 => 'route8',
            1 => 
            array (
              'id' => 'id',
            ),
          ),
        ),
      ),
    ),
  ),
);