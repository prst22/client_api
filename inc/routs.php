<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Inc\Authentication;

// Define app routes

//login/register routs

$app->post('/api/user', function (Request $request, Response $response) {
	$nickname = $request->getParam('nickname');
	$password = $request->getParam('password');
	$token_arr = Authentication::initGenerateToken($request);
	$is_user_exists = Authentication::initLogin($request);

	$user = array(
		'st' 		=> 'Successful login',
		'token'		=> $token_arr['token'],
		'exists'	=> $is_user_exists,
		'username' 	=> $nickname,
		'exp'		=> $token_arr['exp']
	);
	if($user['exists']){
		$jsonResponse = $response->withJson($user);
    	return $jsonResponse->withHeader('Content-Type', 'application/json');
	}else{
		$jsonResponse = $response->withJson(array('error' => 'Wrong username or password'));
    	return $jsonResponse->withHeader('Content-Type', 'application/json');
	}
});

$app->post('/api/singup', function (Request $request, Response $response) {
	$db = new MysqliDb(getenv('DB_HOST'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), getenv('DB_DATABASE'));
	$db->disconnect();
	$new_user = Authentication::initRegister($request);
	$nickname = $request->getParam('nickname');
	$email = $request->getParam('email');
	$password = $request->getParam('password');
	if(strlen($password) < 6){
		$user['error'] = 'Pasword shoud be at least 6 characters';
	    $jsonResponse = $response->withJson($user);
    	return $jsonResponse->withHeader('Content-Type', 'application/json');
	}
	if($new_user){
		$user['st'] = 'New user created';
		$user['username'] = $nickname;
		$user['email'] = $email;
		$user['token'] = Authentication::initGenerateToken($request)['token'];
		$user['exp'] = Authentication::initGenerateToken($request)['exp'];
	}else{
		$db->where("nick", $nickname);	

		if($db->has("users")) {
	    	$user['error'] = 'Username is occupied';
	    	$jsonResponse = $response->withJson($user);
    		return $jsonResponse->withHeader('Content-Type', 'application/json');
		}

		$db->where("email", $email);

		if($db->has("users")) {
	    	$user['error'] = 'Email is occupied';
	    	$jsonResponse = $response->withJson($user);
    		return $jsonResponse->withHeader('Content-Type', 'application/json');
		}
	}

	$jsonResponse = $response->withJson($user);
    return $jsonResponse->withHeader('Content-Type', 'application/json');

});

// display clients rout
$app->redirect('/api/clients', '/api/api/clients/1/new', 301);
$app->redirect('/api/clients/1', '/api/api/clients/1/new', 301);

$app->get('/api/clients/{page}/{sort_by}', function (Request $request, Response $response, $args) {
	empty($args['page']) ? $page = 1 : $page = (int)$args['page'];

    $db = new MysqliDb(getenv('DB_HOST'), getenv('DB_USERNAME'),  getenv('DB_PASSWORD'),  getenv('DB_DATABASE'));
    $db->disconnect();
    switch ($args['sort_by']){
	    case 'new':
	        $db->orderBy("since", "Desc");
	        break;
	    case 'old':
	        $db->orderBy("since", "asc");
	        break;
	    case 'ballance-lowest':
	        $db->orderBy("ballance", "asc");
	        break;
	    case 'ballance-highest':
	        $db->orderBy("ballance", "Desc");
	        break;
	    default:
	    	$db->orderBy("since", "Desc");
	}

	// set page limit to 10 results per page. 20 by default
	$db->pageLimit = 20;
	$clients = $db->arraybuilder()->paginate("clients", $page);
	$totalPages = $db->totalPages;
	$pagesInfo = array();

	if($page <= $totalPages){
		$pagesInfo = array('current_page'	=> strval($page), 'total_pages'	=> strval($totalPages));
	}else{
		$pagesInfo = array('current_page'	=> '', 'total_pages' => strval($totalPages));
	}

	$clientsArr['clients'] = $clients;
	$clientsArr['pages'] = $pagesInfo;

	$jsonResponse = $response->withJson($clientsArr);
	return $jsonResponse->withHeader('Content-Type', 'application/json');
});

// add new item to db
$app->post('/api/add/client', function (Request $request, Response $response){
	$db = new MysqliDb(getenv('DB_HOST'), getenv('DB_USERNAME'),  getenv('DB_PASSWORD'),  getenv('DB_DATABASE'));
	$db->disconnect();
	$nickname = $request->getParam('nickname');
	$email = $request->getParam('email');
	$ballance = number_format((float)$request->getParam('money'), 4, '.', '');
	$token = $request->getAttribute("token");
	$res = array();
	if (in_array("write", $token["scope"])){
		
		try {
			$data = Array (
				"email" 	=> $email,
				"nick" 		=> $nickname,
		        "ballance" 	=> $ballance
			);
			
			$db->where("nick", $nickname);	

			if($db->has("clients")) {
		    	$res['error'] = 'Nickname already exists';
		    	$jsonResponse = $response->withJson($res);
	    		return $jsonResponse->withHeader('Content-Type', 'application/json');
			}

			$db->where("email", $email);	

			if($db->has("clients")) {
		    	$res['error'] = 'Email already exists';
		    	$jsonResponse = $response->withJson($res);
	    		return $jsonResponse->withHeader('Content-Type', 'application/json');
			}

			$new_user = $db->insert('clients', $data);
			if ($new_user){
				$res['st'] = 'New client added'; 
    			$jsonResponse = $response->withJson($res);
    			return $jsonResponse->withHeader('Content-Type', 'application/json');
    		}
		} catch (Exception $e) {
			$res['error'] =  $e->getMessage(); 
    		$jsonResponse = $response->withJson($res);	
    		return $jsonResponse->withHeader('Content-Type', 'application/json');
		}

	}else{

		return $jsonResponse = $response->withStatus(401);

	}
 
});

// update user
$app->put('/api/update/client', function(Request $request, Response $response){
	$db = new MysqliDb(getenv('DB_HOST'), getenv('DB_USERNAME'),  getenv('DB_PASSWORD'),  getenv('DB_DATABASE'));
	$db->disconnect();
	$nickname = $request->getParam('nickname');
	$email = $request->getParam('email');
	$user_id = $request->getParam('id');
	$ballance = number_format((float)$request->getParam('ballance'), 4, '.', '');
	$token = $request->getAttribute("token");
	
	if (in_array("update", $token["scope"])){
		try {
			$data = Array (
				"email" 		=> $email,
				"nick" 			=> $nickname,
		        "ballance" 		=> $ballance
			);
			$success = Array(
				"st" 	=> "Client updated successfully",
			);
			$db->where('id', $user_id);
			$db->update('clients', $data);

			return $jsonResponse = $response->withJson($success)->withHeader('Content-Type', 'application/json');	
		} catch (Exception $e) {
			$error = Array(
				"error" 	=> $e->getMessage(),
			);
	    	return $jsonResponse = $response->withJson($error)->withHeader('Content-Type', 'application/json');
		}
	}else{
		return $jsonResponse = $response->withStatus(401);
	}

});

//delet user
$app->delete('/api/remove/client/{id}', function ($request, $response, $args){
    $db = new MysqliDb(getenv('DB_HOST'), getenv('DB_USERNAME'),  getenv('DB_PASSWORD'),  getenv('DB_DATABASE'));
    $db->disconnect();
   	$token = $request->getAttribute("token");

   	if (in_array("delete", $token["scope"])){
	    try {
	    	$db->where('id', $args['id']);
	    	if($db->delete('clients')){
	    		$success = Array(
					"st" 	=> "Client with id {$args['id']} have been removed",
				);
	    		return $jsonResponse = $response->withJson($success)->withHeader('Content-Type', 'application/json');
	    	}
	    } catch (Exception $e) {
	    	$error = Array(
				"error" 	=> $e->getMessage(),
			);
	    	return $jsonResponse = $response->withJson($error)->withHeader('Content-Type', 'application/json');
	    }
    }else{
    	return $jsonResponse = $response->withStatus(401);
    }	

});