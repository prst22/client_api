<?php
$config = ['settings' => [
    'addContentLengthHeader'    => false,
    'displayErrorDetails'       => false,
    'debug'                     => false,
    'routerCacheFile'           => __DIR__ . '/routes.cache.php',
]];