<?php
namespace Inc;
use Firebase\JWT\JWT;
class Authentication{
	private static function register(object $request){
		$db = new \MysqliDb(getenv('DB_HOST'), getenv('DB_USERNAME'),  getenv('DB_PASSWORD'),  getenv('DB_DATABASE'));
		$db->disconnect();
		$nickname = $request->getParam('nickname');
		$email = $request->getParam('email');
		$password = $request->getParam('password');

		if($nickname == ""){
	    	return false;
		}
		if($password == "" || strlen($password) < 6){
	    	return false;
		}
		if($email == ""){
	    	return false;
		}

		$db->where("nick", $nickname);

		if($db->has("users")){
	    	return false;
		}

		$db->where("email", $email);

		if($db->has("users")){
	    	return false;
		}

		$data = Array(
			"nick" 		=> $nickname,
			"email" 	=> $email,
	        "password" 	=> md5($password)
		);

		$new_user = $db->insert('users', $data);

		if($new_user){
	    	return true;
		}
	}

	public static function initRegister(object $request){
		return Authentication::register($request);
	}

	private static function login(object $request){
		$db = new \MysqliDb(getenv('DB_HOST'), getenv('DB_USERNAME'),  getenv('DB_PASSWORD'),  getenv('DB_DATABASE'));
		$db->disconnect();
		$nickname = $request->getParam('nickname');
		$password = $request->getParam('password');

		if ($db->tableExists('users')){
			$nick = $nickname;
			$password = $password;
			$db->where("nick", $nick);
			$db->where("password", md5($password));

			if($db->has("users")) {
			    return true;
			} else {
			    return false;
			}
		}else{
			return false;
		}
	}

	public static function initLogin(object $request){
		return Authentication::login($request);
	}

	private static function generateToken(object $request){
		$now = new \DateTime();
		$future = new \DateTime("now +1 minutes");
		$secret = getenv('JWT_SECRET');
		$payload = [
		 	"iat" 	=> $now->getTimeStamp(),
		 	"exp" 	=> $future->getTimeStamp(),
			"jti" 	=> base64_encode(random_bytes(16)),
			"scope" => ["update", "write", "delete"]
		];
		$token = JWT::encode($payload, $secret, "HS256");
		return array(
			'token' 	=> $token,
			'exp' 		=> $future->getTimeStamp()
		);
     }

    public static function initGenerateToken(object $request){
    	return Authentication::generateToken($request);
    }
}